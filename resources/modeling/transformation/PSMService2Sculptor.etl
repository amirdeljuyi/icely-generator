import "../utilities/ServicePSM.eol";
import "../utilities/SculptorPSM.eol";

pre {
	"Running ETL Transformation: ".println();
	"----------------------------".println();
	"".println();
	var serviceModel;
	var sculptorModel;
}

rule Project transform c:ServicePSM!Project
 	to a:Sculptor!Project{
 	guard: c.applications.size() > 0
 	"-------------- Project --------------".println();
 	a.name = c.name;
	serviceModel = c;
 	sculptorModel = a;
}


rule Application transform c:ServicePSM!Application
 	to a:Sculptor!Application{
 	"-------------- Application --------------".println();
 	a.name = c.name.firstToUpperCase();
 	a.basePackage = "org."+ c.name.firstToLowerCase();
 	a.project = sculptorModel;
 	sculptorModel.applications.add(a);
 	
}

rule Module transform c:ServicePSM!Module
 	to a:Sculptor!Module{
 	"-------------- Application --------------".println();
 	a.name = c.name;
 	a.external = c.external;
 	a.basePackage = c.basePackage;
 	a.persistenceUnit = c.persistenceUnit;
	a.application = c.application.equivalent(); 
	a.application.modules.add(a);
}



rule Property2Attribute transform c:ServicePSM!Property
	to a:Sculptor!Attribute{
	"-------------- Attribute --------------".println();
	a.name = c.name;
	a.changeable = c.changeable;
	a.nullable = c.nullable;
	a.visibility = c.visibility;
	a.required = c.required;
	a.validate = c.validate;
	a.length = c.length;
	a.transient = c.transient;
	if(c.type.type().name = "ComplexType"){
		a.type = c.type.type.typeName;
		a.collectionType = c.type.collection.name.toLowerCase().firstToUpperCase();
	} else {
		a.type = c.type.typeName;
	}
	// TODO: CollectionType and MapType
}
 

@abstract
rule DomainObjectAbstract transform c:ServicePSM!DomainObject
	to a:Sculptor!DomainObject{	
	// attributes
	a.name = c.name;
	a.abstract = c.abstract;
	a.aggregateRoot = c.aggregateRoot;
	if(not a.aggregateRoot){
		a.belongsToAggregate = c.aggregate.aggregateRoot.equivalent();
	}
	a.optimisticLocking = c.optimisticLocking;
	a.cache = c.cache;
	a.gapClass = c.gapClass;
	
	// references
	// a.hasInheritance ::= c.hasInheritance;
	a.attributes ::= c.properties;
	a.attributes.println();

	a.module = c.aggregate.module.equivalent();
	a.module.domainObjects.add(a);
}

rule Entity transform c:ServicePSM!Entity
	to a:Sculptor!Entity extends DomainObjectAbstract {
	"-------------- entity --------------".println();
	a.auditable = c.auditable;
	// a.scaffold = c.scaffold;	
}


@abstract
rule ValueObjectAbstract transform c:ServicePSM!ValueObject
	to a:Sculptor!ValueObject extends DomainObjectAbstract {
	c.println();
	a.immutable = c.immutable;
	a.persistent = c.persistent;
}


rule BasicType transform c:ServicePSM!BasicType
	to a:Sculptor!BasicType extends ValueObjectAbstract{
	"---------------- BasicType --------------".println();
	c.println();
}


rule DomainEvent transform c:ServicePSM!DomainEvent
	to a:Sculptor!DomainEvent extends ValueObjectAbstract{
	"---------------- DomainEvent --------------".println();
	c.println();
}


rule CommandEvent transform c:ServicePSM!CommandEvent
	to a:Sculptor!CommandEvent extends ValueObjectAbstract{
	"---------------- CommandEvent --------------".println();
	c.println();
}


rule ValueObject transform c:ServicePSM!ValueObject
	to a:Sculptor!ValueObject extends ValueObjectAbstract{
	"---------------- ValueObject --------------".println();
	c.println();
}

rule DTO transform c:ServicePSM!DTO
	to a:Sculptor!DataTransferObject extends ValueObjectAbstract{
	"---------------- DTO --------------".println();
	c.println();
}

rule Literal2EnumValue transform c:ServicePSM!Literal
	to a:Sculptor!EnumValue{
	"---------------- Literal --------------".println();
	a.name = c.name;
	var enumParam = new Sculptor!EnumConstructorParameter;
	enumParam.value = c.value;
	a.parameters.add(enumParam);
}

rule Enumeration2Enum transform c:ServicePSM!Enumeration
	to a:Sculptor!Enum extends DomainObjectAbstract{
	"---------------- enumeration --------------".println();
	a.values ::= c.literals;
}

rule Relation2Reference transform c:ServicePSM!Relation
	to a:Sculptor!Reference{
	c.type.type().println();
	if (c.type.type().name = "ComplexType"){
		a.many = true;
		a.collectionType = c.type.collection.name.toLowerCase().firstToUpperCase();
	} else {
		a.many = false;
	}
	a.name = c.name;
	a.changeable = c.changeable;
	a.required = c.required;
		
	a.from = c.source.equivalent();
	"source".println();
	a.from.println();
	a.`to` = c.target.equivalent();
	a.from.references.add(a);
}

rule Publish transform c:ServicePSM!Publish
	to a:Sculptor!Publish{
	"------------- Publish --------------".println();
	a.eventType = c.eventType.equivalent();
	a.eventBus = c.eventBus;
	a.topic = c.topic;
}

rule Subscribe transform c:ServicePSM!Subscribe
	to a:Sculptor!Subscribe{
	"------------- Subscribe --------------".println();
	a.eventBus = c.eventBus;
	a.topic = c.topic;
}

rule Resource transform c:ServicePSM!Resource
	to a:Sculptor!Resource{
	a.name = c.name;
	a.path = c.path.relativePath;
	a.gapClass = c.gapClass;
	a.module = c.module.equivalent();
}

rule Service transform c:ServicePSM!Service
	to a:Sculptor!Service{
	"-------------- Service --------------".println();
	a.name = c.name;
	a.otherDependencies ::= c.otherDependencies;
	a.webService = c.webService;
	a.remoteInterface = c.remoteInterface;
	a.localInterface = c.localInterface;
	a.gapClass = c.gapClass;
	a.module = c.module.equivalent();
	a.subscribe ::= c.subscribe;
}

rule Repository transform  c:ServicePSM!Repository
	to a:Sculptor!Repository{
		"-------------- Repository --------------".println();
	a.name = c.name;
	a.gapClass = c.gapClass;
	a.otherDependencies ::= c.otherDependencies;
	a.aggregateRoot = c.aggregateRoot.equivalent();
	a.aggregateRoot.repository = a;
	a.subscribe ::= c.subscribe;
}

rule Parameter transform c:ServicePSM!Parameter
	to a:Sculptor!Parameter{
	"-------------- Parameter --------------".println();
	a.name = c.name;
	var collection = null;
	var map = null;
	var name = null;
	if( c.type.type().name == "ComplexType" ){
		collection = c.type.collection.name;
		map = c.type.map == null ? null : c.type.map.type.name;
		name = c.type.type.name;
	} else{
		name = c.type.typeName;
	}
	a.setTypedElement(name, collection, map);
}

@abstract
rule OperationAbstract  transform c:ServicePSM!Operation
	to a:Sculptor!Operation{
	a.name = c.name;
	
	if(c.returntype != null){
		var collection = null;
		var map = null;
		if( c.returntype.type().name == "ComplexType" ){
			collection = c.type.collection.name;
			map = c.type.map == null ? null : c.type.map.type.name;
			name = c.type.type.name;
		} else{
			name = c.type.typeName;
		}
		a.setTypedElement(name, collection, map);
	}
	a.parameters ::= c.parameters;
	a.publish ::= c.publish;
}

rule RepositoryOperation transform c:ServicePSM!RepositoryOperation
	to a:Sculptor!RepositoryOperation extends OperationAbstract{
	"-------------- ServiceOperation --------------".println();
	a.delegateToAccessObject = c.delegateToAccessObject;
	a.accessObjectName = c.accessObjectName;
}

rule ServiceOperation transform c:ServicePSM!ServiceOperation
	to a:Sculptor!ServiceOperation extends OperationAbstract{
	"-------------- ServiceOperation --------------".println();
	
	a.service = c.service.equivalent();
	a.service.operations.add(a);
	a.serviceDelegate = c.serviceDelegate;
}

rule ResourceOperation transform c:ServicePSM!ResourceOperation
	to a:Sculptor!ResourceOperation extends OperationAbstract{
	"-------------- ResourceOperation --------------".println();
	a.path = c.path;
	a.httpMethod ::= c.httpMethod;
	a.returnString = c.returnString;
	a.resource ::= c.resource;
	a.resource.operations.add(a);
	a.delegate ::= c.delegate;
}

post {
}