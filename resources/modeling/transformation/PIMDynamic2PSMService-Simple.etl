import "../utilities/DynamicPIM.eol";
import "../utilities/ServicePSM.eol";
import "./PIMDynamic2PSMService.etl";


pre {
	"Running ETL Transformation: ".println();
	"----------------------------".println();
	"".println();
	var pimModel;
	var psmModel;
}

rule Project transform c:DynamicPIM!Project
 	to a:ServicePSM!Project extends AbstractProject{
 	guard: c.hasApplication.size() > 0
	pimModel = c;
 	psmModel = a;
}


rule Application transform c:DynamicPIM!Application
	to a:ServicePSM!Application extends AbstractApplication{
	a.addModule("command");
	a.addModule("^query");
	a.addModule("rest");
}

/*
rule primitiveType2primitiveType transform c:DynamicPIM!PrimitiveType
	to a:ServicePSM!PrimitiveType{
	a.type ::= c.type;
	a.typeName = c.typeName;
	psmModel.primitivetype.add(a);
}
*/


rule Aggregate transform c:DynamicPIM!Aggregate 
	to a:ServicePSM!Aggregate extends AbstractAggregate{
	var application = c.application.equivalent();
	
	var queryAggregate = new ServicePSM!Aggregate;
	queryAggregate.name = a.name;
	queryAggregate.module = application.getQueryModule();
	queryAggregate.module.aggregates.add(queryAggregate);
	
	a.module = application.getCommandModule();
	a.module.aggregates.add(a);
}

rule BasicType transform c:DynamicPIM!BasicType
	to a:ServicePSM!BasicType extends AbstractBasicType{
	var aggregate = c.aggregate.equivalent();
	var application = c.aggregate.application.equivalent();
	a.addDomainObjectToCommandAggregate(application, aggregate.name);
}


rule DomainEvent transform c:DynamicPIM!DomainEvent
	to a:ServicePSM!DomainEvent extends AbstractDomainEvent{
	var aggregate = c.aggregate.equivalent();
	var application = c.aggregate.application.equivalent();
	a.addDomainObjectToCommandAggregate(application, aggregate.name);
}


rule CommandEvent transform c:DynamicPIM!CommandEvent
	to a:ServicePSM!CommandEvent extends AbstractCommandEvent{
	var aggregate = c.aggregate.equivalent();
	var application = c.aggregate.application.equivalent();
	a.addDomainObjectToCommandAggregate(application, aggregate.name);
}

rule DTO transform c:DynamicPIM!DTO
	to a:ServicePSM!DTO extends AbstractDTO{
	var aggregate = c.aggregate.equivalent();
	var application = c.aggregate.application.equivalent();
	a.addDomainObjectToQueryAggregate(application, aggregate.name);
}


rule ValueObject transform c:DynamicPIM!ValueObject
	to a:ServicePSM!ValueObject extends AbstractValueObject{
	var aggregate = c.aggregate.equivalent();
	var application = c.aggregate.application.equivalent();
	a.addDomainObjectToCommandAggregate(application, aggregate.name);
}

rule Entity transform c:DynamicPIM!Entity
	to a:ServicePSM!Entity extends AbstractEntity{
	var aggregate = c.aggregate.equivalent();
	var application = c.aggregate.application.equivalent();
	a.addDomainObjectToCommandAggregate(application, aggregate.name);
	
	// query entity
	if (not a.`abstract`){
		var queryEntity = new ServicePSM!Entity;
		queryEntity.name = a.name + "Query";
		queryEntity.typeName = a.typeName+ "Query";
		queryEntity.aggregateRoot = a.aggregateRoot;
		
		var saveOperation = a.repository.addOperation("save", null, null);
		saveOperation.addPublish(null, a.name + "StoreChannel");
		
		queryEntity.addRepository();
		queryEntity.repository.addSubscribe(null, a.name + "StoreChannel");
		queryEntity.addDomainObjectToQueryAggregate(application, aggregate.name);
	}
}

rule Enumeration transform c:DynamicPIM!Enumeration
	to a:ServicePSM!Enumeration extends AbstractEnumeration{
	var aggregate = c.aggregate.equivalent();
	var application = c.aggregate.application.equivalent();
	a.addDomainObjectToCommandAggregate(application, aggregate.name);
}

@greedy
rule Resource transform c:DynamicPIM!Resource
	to a:ServicePSM!Resource extends AbstractResource{
}

rule Process2Service transform c:DynamicPIM!Process
	to a:ServicePSM!Service extends AbstractProcess2Service{
}
	
@greedy
rule ProcessOperation2ServiceOperation transform c:DynamicPIM!ProcessOperation
	to a:ServicePSM!ServiceOperation extends AbstractProcessOperation2ServiceOperation{
	
}

rule Read2resourceOperation transform c:DynamicPIM!Read
	to a:ServicePSM!ResourceOperation extends AbstractResourceActivity2ResourceOperation{
}

rule Create2resourceOperation transform c:DynamicPIM!Create
	to a:ServicePSM!ResourceOperation extends AbstractResourceActivity2ResourceOperation{
}

rule Other2ResourceOperation transform c:DynamicPIM!Other
	to a:ServicePSM!ResourceOperation extends AbstractResourceActivity2ResourceOperation{
}

rule Delete2ResourceOperation transform c:DynamicPIM!Delete
	to a:ServicePSM!ResourceOperation extends AbstractResourceActivity2ResourceOperation{
}

post {
}
