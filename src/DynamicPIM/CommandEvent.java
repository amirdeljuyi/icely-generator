/**
 */
package DynamicPIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see DynamicPIM.DynamicPIMPackage#getCommandEvent()
 * @model
 * @generated
 */
public interface CommandEvent extends Event {
} // CommandEvent
