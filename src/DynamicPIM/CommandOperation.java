/**
 */
package DynamicPIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see DynamicPIM.DynamicPIMPackage#getCommandOperation()
 * @model
 * @generated
 */
public interface CommandOperation extends ProcessOperation {
} // CommandOperation
