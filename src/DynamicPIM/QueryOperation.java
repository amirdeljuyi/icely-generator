/**
 */
package DynamicPIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see DynamicPIM.DynamicPIMPackage#getQueryOperation()
 * @model
 * @generated
 */
public interface QueryOperation extends ProcessOperation {
} // QueryOperation
