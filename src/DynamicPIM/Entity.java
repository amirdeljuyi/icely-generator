/**
 */
package DynamicPIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see DynamicPIM.DynamicPIMPackage#getEntity()
 * @model
 * @generated
 */
public interface Entity extends DomainObject {
} // Entity
