/**
 */
package DynamicPIM.impl;

import DynamicPIM.DomainEvent;
import DynamicPIM.DynamicPIMPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Domain Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DomainEventImpl extends EventImpl implements DomainEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DomainEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DynamicPIMPackage.Literals.DOMAIN_EVENT;
	}

} //DomainEventImpl
