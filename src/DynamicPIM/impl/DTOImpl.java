/**
 */
package DynamicPIM.impl;

import DynamicPIM.DTO;
import DynamicPIM.DynamicPIMPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DTO</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DTOImpl extends ValueObjectImpl implements DTO {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DTOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DynamicPIMPackage.Literals.DTO;
	}

} //DTOImpl
