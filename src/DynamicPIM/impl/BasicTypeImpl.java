/**
 */
package DynamicPIM.impl;

import DynamicPIM.BasicType;
import DynamicPIM.DynamicPIMPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Basic Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BasicTypeImpl extends ValueObjectImpl implements BasicType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BasicTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DynamicPIMPackage.Literals.BASIC_TYPE;
	}

} //BasicTypeImpl
