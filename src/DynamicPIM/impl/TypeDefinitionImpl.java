/**
 */
package DynamicPIM.impl;

import DynamicPIM.DynamicPIMPackage;
import DynamicPIM.TypeDefinition;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class TypeDefinitionImpl extends EObjectImpl implements TypeDefinition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DynamicPIMPackage.Literals.TYPE_DEFINITION;
	}

} //TypeDefinitionImpl
