/**
 */
package DynamicPIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DTO</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see DynamicPIM.DynamicPIMPackage#getDTO()
 * @model
 * @generated
 */
public interface DTO extends ValueObject {
} // DTO
