/**
 */
package DynamicPIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Other</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see DynamicPIM.DynamicPIMPackage#getOther()
 * @model
 * @generated
 */
public interface Other extends ResourceActivity {
} // Other
