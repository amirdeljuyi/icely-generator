/**
 */
package DynamicPIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Collection Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see DynamicPIM.DynamicPIMPackage#getCollectionResource()
 * @model
 * @generated
 */
public interface CollectionResource extends Resource {
} // CollectionResource
