/**
 */
package StaticPIM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hi</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see StaticPIM.StaticPIMPackage#getHi()
 * @model
 * @generated
 */
public interface Hi extends EObject {
} // Hi
