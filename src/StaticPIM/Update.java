/**
 */
package StaticPIM;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Update</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see StaticPIM.StaticPIMPackage#getUpdate()
 * @model
 * @generated
 */
public interface Update extends CRUDActivity {
} // Update
