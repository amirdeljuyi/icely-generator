/**
 */
package StaticPIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see StaticPIM.StaticPIMPackage#getBasicType()
 * @model
 * @generated
 */
public interface BasicType extends ValueObject {
} // BasicType
