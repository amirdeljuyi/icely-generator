/**
 */
package StaticPIM.impl;

import StaticPIM.BasicType;
import StaticPIM.StaticPIMPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Basic Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BasicTypeImpl extends ValueObjectImpl implements BasicType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BasicTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StaticPIMPackage.Literals.BASIC_TYPE;
	}

} //BasicTypeImpl
