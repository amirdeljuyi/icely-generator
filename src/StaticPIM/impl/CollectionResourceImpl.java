/**
 */
package StaticPIM.impl;

import StaticPIM.CollectionResource;
import StaticPIM.StaticPIMPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Collection Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CollectionResourceImpl extends ResourceImpl implements CollectionResource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CollectionResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StaticPIMPackage.Literals.COLLECTION_RESOURCE;
	}

} //CollectionResourceImpl
