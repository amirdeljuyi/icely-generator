/**
 */
package StaticPIM.impl;

import StaticPIM.CommandEvent;
import StaticPIM.StaticPIMPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CommandEventImpl extends EventImpl implements CommandEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommandEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StaticPIMPackage.Literals.COMMAND_EVENT;
	}

} //CommandEventImpl
