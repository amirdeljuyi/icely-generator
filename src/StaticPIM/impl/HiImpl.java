/**
 */
package StaticPIM.impl;

import StaticPIM.Hi;
import StaticPIM.StaticPIMPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hi</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class HiImpl extends MinimalEObjectImpl.Container implements Hi {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HiImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StaticPIMPackage.Literals.HI;
	}

} //HiImpl
