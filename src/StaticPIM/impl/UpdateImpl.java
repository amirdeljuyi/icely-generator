/**
 */
package StaticPIM.impl;

import StaticPIM.StaticPIMPackage;
import StaticPIM.Update;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Update</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UpdateImpl extends CRUDActivityImpl implements Update {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UpdateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StaticPIMPackage.Literals.UPDATE;
	}

} //UpdateImpl
