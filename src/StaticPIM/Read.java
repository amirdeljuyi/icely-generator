/**
 */
package StaticPIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Read</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see StaticPIM.StaticPIMPackage#getRead()
 * @model
 * @generated
 */
public interface Read extends CRUDActivity {
} // Read
