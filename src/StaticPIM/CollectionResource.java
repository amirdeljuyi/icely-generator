/**
 */
package StaticPIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Collection Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see StaticPIM.StaticPIMPackage#getCollectionResource()
 * @model
 * @generated
 */
public interface CollectionResource extends Resource {
} // CollectionResource
