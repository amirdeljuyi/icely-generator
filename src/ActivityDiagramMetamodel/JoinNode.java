/**
 */
package ActivityDiagramMetamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Join Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ActivityDiagramMetamodel.ActivityDiagramMetamodelPackage#getJoinNode()
 * @model annotation="gmf.node tool.name='Join Node' figure='svg' svg.uri='platform:/plugin/MyCore/svg/vertical-line.svg' label.icon='false' label.placement='none' size='10,80' resizable='false' margin='2'"
 * @generated
 */
public interface JoinNode extends ControlNode {
} // JoinNode
