/**
 */
package ActivityDiagramMetamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Final Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ActivityDiagramMetamodel.ActivityDiagramMetamodelPackage#getFinalNode()
 * @model annotation="gmf.node tool.name='Final Node' figure='svg' svg.uri='platform:/plugin/MyCore/svg/rec.svg' label.icon='false' label.placement='none' size='30,60' resizable='false' margin='2'"
 * @generated
 */
public interface FinalNode extends ControlNode {
} // FinalNode
