/**
 */
package ActivityDiagramMetamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External System</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ActivityDiagramMetamodel.ActivityDiagramMetamodelPackage#getExternalSystem()
 * @model annotation="gmf.node label='name' color='231,147,186'"
 * @generated
 */
public interface ExternalSystem extends Supplier {
} // ExternalSystem
