/**
 */
package ActivityDiagramMetamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ActivityDiagramMetamodel.ActivityDiagramMetamodelPackage#getQueryAction()
 * @model annotation="gmf.node label='name' color='108,198,192'"
 * @generated
 */
public interface QueryAction extends Action {
} // QueryAction
