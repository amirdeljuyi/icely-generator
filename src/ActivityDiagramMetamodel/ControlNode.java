/**
 */
package ActivityDiagramMetamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ActivityDiagramMetamodel.ActivityDiagramMetamodelPackage#getControlNode()
 * @model annotation="gmf.node label.placement='none'"
 * @generated
 */
public interface ControlNode extends ActivityNode {
} // ControlNode
