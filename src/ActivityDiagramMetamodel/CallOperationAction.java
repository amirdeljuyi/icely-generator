/**
 */
package ActivityDiagramMetamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Operation Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ActivityDiagramMetamodel.ActivityDiagramMetamodelPackage#getCallOperationAction()
 * @model
 * @generated
 */
public interface CallOperationAction extends CallAction {
} // CallOperationAction
