/**
 */
package ActivityDiagramMetamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ActivityDiagramMetamodel.ActivityDiagramMetamodelPackage#getActor()
 * @model annotation="gmf.node label='name' label.placement='internal' color='255,249,178'"
 * @generated
 */
public interface Actor extends ActivityPartition {

} // Actor
