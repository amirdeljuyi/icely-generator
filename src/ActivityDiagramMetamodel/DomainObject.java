/**
 */
package ActivityDiagramMetamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Domain Object</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ActivityDiagramMetamodel.ActivityDiagramMetamodelPackage#getDomainObject()
 * @model annotation="gmf.node label='name'"
 * @generated
 */
public interface DomainObject extends ObjectNode {
} // DomainObject
