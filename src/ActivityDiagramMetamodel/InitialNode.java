/**
 */
package ActivityDiagramMetamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Initial Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ActivityDiagramMetamodel.ActivityDiagramMetamodelPackage#getInitialNode()
 * @model annotation="gmf.node tool.name='Initial Node' figure='svg' svg.uri='platform:/plugin/MyCore/svg/full-moon.svg' label.icon='false' label.placement='none' size='30,30' resizable='false' margin='2'"
 * @generated
 */
public interface InitialNode extends ControlNode {
} // InitialNode
