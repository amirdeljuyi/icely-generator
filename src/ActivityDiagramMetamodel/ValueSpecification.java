/**
 */
package ActivityDiagramMetamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ActivityDiagramMetamodel.ActivityDiagramMetamodelPackage#getValueSpecification()
 * @model
 * @generated
 */
public interface ValueSpecification extends EObject {
} // ValueSpecification
